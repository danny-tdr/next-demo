This NextJS demo contains:

- index, collection & product pages:
  Demoing how to fetch data from the API and displaying the data.

- src/components/\*
  example reusable components & css

- lib/hooks/useStorefrontProduct
  This looks up the product data using the storefront API, and updates the props if they've changed

- lib/storefront/\*
  Graphql queries to fetch data from the storefront api

- lib/slice
  global redux state to manage the cart. the state gets written to localstore so it persists between reloads
