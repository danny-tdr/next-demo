const url = process.env.NEXT_PUBLIC_STORE + '/api/2021-07/graphql.json'

const client =
  ([query]) =>
  responseHandler =>
  variables =>
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-Shopify-Storefront-Access-Token':
          process.env.NEXT_PUBLIC_STOREFRONT_ACCESS_TOKEN
      },
      body: JSON.stringify({
        query,
        variables
      })
    })
      .then(res => res.json())
      .then(res => {
        if (res.errors) {
          throw new Error(res.errors[0].message)
        }
        return res.data
      })
      .then(responseHandler)

export default client
