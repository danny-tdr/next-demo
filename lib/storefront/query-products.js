import gql from './client'

// example input
const variables = {
  id: 'cfd1b179ad159fd62741cceb333a7ee7'
}

// https://shopify.dev/api/storefront/reference/common-objects/node
// https://shopify.dev/api/storefront/reference/products/product
const query = gql`
  query ($id: ID!) {
    node(id: $id) {
      id
      ... on Product {
        id
        title
        handle
        variants(first: 20) {
          edges {
            node {
              id
              price
              title
            }
          }
        }
      }
    }
  }
`

const responseHandler = async res => {
  const product = res.node
  product.variants = product.variants.edges.map(e => ({ ...e.node }))

  return product
}

export default query(responseHandler)
