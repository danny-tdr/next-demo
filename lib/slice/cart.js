import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

const initialState = {
  items: {}
}

// slice
export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addItem: (state, action) => {
      const { id, quantity, ...product } = action.payload
      if (!state.items[id]) {
        state.items[id] = {
          ...product,
          id,
          quantity: 0
        }
      }
      state.items[id].quantity += quantity
    },
    setQuantity: (state, action) => {
      const { id, quantity } = action.payload
      state.items[id].quantity = quantity
    },
    removeItem: (state, action) => {
      const { id } = action.payload
      delete state.items[id]
    }
  }
})

// selectors
export const itemsSelector = state => state.cart.items

export const itemsCountSelector = state => {
  const { items } = state.cart
  const count = Object.values(items).reduce(
    (n, item) => n + Number(item.quantity),
    0
  )
  return count
}

export const totalPriceSelector = state => {
  const { items } = state.cart
  const total = Object.values(items).reduce(
    (n, item) => n + item.quantity * item.price,
    0
  )
  return total
}

// async
export const checkout = createAsyncThunk(
  'cart/checkout',
  async (args, { getState }) => {
    const { router } = args
    const { cart } = getState()

    const data = {
      items: Object.values(cart.items)
    }

    fetch(`${process.env.NEXT_PUBLIC_API}/checkout`, {
      method: 'POST',
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(res => {
        router.push(res.url)
      })
  }
)

export const { addItem, setQuantity, removeItem } = cartSlice.actions
export default cartSlice.reducer
