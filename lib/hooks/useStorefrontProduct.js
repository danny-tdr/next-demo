import { useEffect, useState } from 'react'
import queryProducts from '@/lib/storefront/query-products'

const useStorefrontProduct = initialProduct => {
  const [product, setProduct] = useState(initialProduct)

  const variables = {
    id: Buffer.from('gid://shopify/Product/' + initialProduct.id).toString(
      'base64'
    )
  }

  useEffect(() => {
    queryProducts(variables).then(storefrontProduct => {
      setProduct({
        ...product,
        ...storefrontProduct
      })
    })
  }, [])

  return product
}

export default useStorefrontProduct
