import { configureStore } from '@reduxjs/toolkit'
import { saveState, loadState } from './localStorage'
import throttle from 'lodash.throttle'
import cart from './slice/cart'

export const store = configureStore({
  reducer: {
    cart
  },
  preloadedState: loadState()
})

store.subscribe(throttle(() => saveState(store.getState()), 1000))
