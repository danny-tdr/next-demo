const priceFormat = new Intl.NumberFormat('en-GB', {
  style: 'currency',
  currency: 'GBP'
})

const price = number => priceFormat.format(number)

export default price
