import styles from '@/styles/components/layout.module.css'
import Header from './header'
import Footer from './footer'

export default function Layout({ children, ...props }) {
  return (
    <>
      <Header />
      <main className={styles.main}>{children}</main>
      <Footer />
    </>
  )
}
