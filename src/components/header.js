import Link from 'next/link'
import styles from '@/styles/components/header.module.css'

const Header = () => (
  <div className={styles.root}>
    <Link href="/">Homepage</Link>
    <Link href="/cart">Cart</Link>
  </div>
)

export default Header
