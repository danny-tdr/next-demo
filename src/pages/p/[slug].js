import { useState } from 'react'
import useStorefrontProduct from '@/lib/hooks/useStorefrontProduct'
import { useDispatch } from 'react-redux'
import { addItem } from '@/lib/slice/cart'

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking'
  }
}

export async function getStaticProps(context) {
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_API}/products/${context.params.slug}`
  )

  if (res.status != 200) {
    return {
      notFound: true
    }
  }

  const props = await res.json()

  if (!props) {
    return {
      notFound: true
    }
  }

  return {
    props,
    revalidate: 60
  }
}

export default function Product(props) {
  const dispatch = useDispatch()
  const product = useStorefrontProduct(props)
  const [selected, setSelected] = useState(0)

  const add = () => {
    const variant = product.variants[selected]

    dispatch(
      addItem({
        handle: product.handle,
        collections: product.collections,
        title: product.title,
        image: product.images[0],
        id: variant.id,
        variant: variant.title,
        price: variant.price,
        quantity: 1
      })
    )
  }

  return (
    <div>
      <h1>{product.title}</h1>

      <select value={selected} onChange={e => setSelected(e.target.value)}>
        {product.variants.map((v, i) => (
          <option key={v.id} value={i}>
            {v.title} - £{v.price}
          </option>
        ))}
      </select>

      <button onClick={add}>Add to cart</button>
    </div>
  )
}
