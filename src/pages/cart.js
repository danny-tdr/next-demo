import { useSelector } from 'react-redux'
import { totalPriceSelector, itemsSelector } from '@/lib/slice/cart'
import price from '@/lib/price'

export default function Cart() {
  const items = useSelector(itemsSelector)
  const total = price(useSelector(totalPriceSelector))

  if (!Object.values(items).length) {
    return (
      <div>
        <h2>Cart</h2>
        <p>Cart is empty</p>
      </div>
    )
  }

  return (
    <div>
      {Object.values(items).map((item, i) => (
        <div key={i}>
          <div>{item.title}</div>
          <div>Quantity {item.quantity}</div>
          <div>Price £{item.price}</div>

          <br />
        </div>
      ))}

      <div>Total: {total}</div>
    </div>
  )
}
