import Link from 'next/link'

export async function getStaticProps() {
  const collections = await fetch(
    `${process.env.NEXT_PUBLIC_API}/collections`
  ).then(res => res.json())

  return {
    props: { collections },
    revalidate: 60
  }
}

export default function Home(props) {
  return (
    <div>
      {props.collections.slice(0, 12).map(collection => (
        <Link key={collection.handle} href={`/c/${collection.handle}`}>
          <a style={{ display: 'block' }}>{collection.handle}</a>
        </Link>
      ))}
    </div>
  )
}
