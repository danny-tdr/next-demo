import Link from 'next/link'

export async function getStaticPaths() {
  const res = await fetch(`${process.env.NEXT_PUBLIC_API}/collections`)
  const collections = await res.json()

  return {
    paths: collections
      .map(collection => ({
        params: {
          slug: collection.handle
        }
      }))
      // just so that we dont prerender all of the pages as once
      .slice(0, 12),
    fallback: 'blocking'
  }
}

export async function getStaticProps(context) {
  const products = await fetch(
    `${process.env.NEXT_PUBLIC_API}/collections/${context.params.slug}`
  ).then(res => res.json())

  return {
    props: products,
    revalidate: 60
  }
}

export default function Home(props) {
  return (
    <div>
      {props.products.map(product => (
        <Link key={product.handle} href={`/p/${product.handle}`}>
          <a style={{ display: 'block' }}>{product.title}</a>
        </Link>
      ))}
    </div>
  )
}
